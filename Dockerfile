FROM maven:3.6.0-jdk-8-alpine
COPY . .
RUN mvn test \
    && mvn clean package
WORKDIR target
CMD ["java", "-jar", "my-app-1.0-SNAPSHOT.jar"]
